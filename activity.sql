INSERT INTO users (email, password, datetime_created) VALUES(
    "johnsmith@gmail.com",
    "passwordA",
    "2021-01-02 01:00:00"
);
INSERT INTO users (email, password, datetime_created) VALUES(
    "juandelacruz@gmail.com",
    "passwordB",
    "2021-01-02 02:00:00"
);
INSERT INTO users (email, password, datetime_created) VALUES(
    "janesmith@gmail.com",
    "passwordC",
    "2021-01-02 03:00:00"
);
INSERT INTO users (email, password, datetime_created) VALUES(
    "mariadelacruz@gmail.com",
    "passwordD",
    "2021-01-02 04:00:00"
);
INSERT INTO users (email, password, datetime_created) VALUES(
    "johndoe@gmail.com",
    "passwordD",
    "2021-01-02 05:00:00"
);


INSERT INTO posts(author_id, title, content, datetime_posted) VALUES(
    1,
    "First Code",
    "Hello World!",
    "2021-01-02 01:00:00"

);
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES(
    1,
    "Second Code",
    "Hello Earth!",
    "2021-01-02 02:00:00"
);
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES(
    2,
    "Third Code",
    "Welcome to Mars!",
    "2021-01-02 03:00:00"
);
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES(
    4,
    "Fourth Code",
    "Bye bye solar system!",
    "2021-01-02 04:00:00"
);

SELECT content FROM posts WHERE author_id = 1;
SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello" WHERE title = "Second Code";

DELETE  FROM users WHERE email = "johndoe@gmail.com";

